from app import *


#homepage
@app.route('/')
def app_index():
  #has logged in -> view user home
  if hasLoggedIn():
    return redirect(url_for('feed_index'))

  #not logged in yet -> view log-in page
  else:
    view = 'app/index.html'
    return render_template(view)



#clear session util
@app.route('/clear-session')
def app_clear_session():
  session.clear()
  return redirect(url_for('app_index') )
