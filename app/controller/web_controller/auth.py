from app import *


#required log-in setup
@app.before_request
def app_before_request():
  #if not logged-in, show log-in page
  if not hasLoggedIn():
    ##region bypass cases

    #bypass if current endpoint came from api blueprint
    try:
      blueprint = request.endpoint.split('.')[0]
    except:
      blueprint = None #no blueprint in endpoint, set it none

    from app.controller.api_controller import API_BLUEPRINT_NAME
    bypass = blueprint == API_BLUEPRINT_NAME

    if bypass: return


    #bypass if current endpoint is sign-in page itself
    bypass = request.endpoint == 'auth_sign_in'
    if bypass: return

    #bypass if current endpoint is a static-asset request
    bypass = request.endpoint == 'static'
    if bypass: return

    ##endregion bypass cases

    return redirect(url_for('auth_sign_in'))


@app.route('/auth/sign-in')
def auth_sign_in():
  if hasLoggedIn():
    return redirect(url_for('user_home') )

  return render_template('auth/sign-in.html')


@app.route('/auth/sign-out')
def auth_sign_out():
  #mark as logged out
  if 'authInfo' in session:
    del session['authInfo']

  #render view
  return redirect(url_for('app_index'))

