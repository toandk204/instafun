from app import *

##region hello demo
pass


#the listener - html page with js code to register listener to our 'sse' instance; at listener type=hello
@app.route('/sse/listen-hello')
def sse_listener_hello():
  return render_template('sse/listen-hello.html', **locals())


#the publisher
@app.route('/sse/publish-hello')
def sse_publish_hello():
  SSE_INSTANCE.publish({"message": "Hello!"}, type='greeting')
  return "Message sent!"


pass
##endregion hello demo
