from app import *
from app.model.post import Post


@app.route('/feed')
def feed_index():
  return render_template('feed/index.html', **locals())


@app.route('/feed/full-screen')
def feed_index_full_screen():
  full_screen_post_id = requireValueFromRequest(request, 'post_id', allowEmpty=True)
  if not full_screen_post_id: return redirect(url_for('feed_index'))

  full_screen_post = Post.query.get(full_screen_post_id)
  if not full_screen_post: return redirect(url_for('feed_index'))

  return render_template('feed/index.html', **locals())
