from common import *
from app import *


@api_blueprint.route('/sse/publish-post-update', methods=['POST'])
def sse_post_update():
  #do publish
  eventType,eventMsg = sse_publishPostUpdate(SSE_INSTANCE)

  #output
  r = ApiResult()
  r.status = 1
  r.message = 'Published event %s' % eventType
  r.data = eventMsg
  return r.toJSON()
