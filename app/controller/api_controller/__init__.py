#region create blueprint
"""ref. http://stackoverflow.com/a/24420993/248616"""
from flask import Blueprint
API_BLUEPRINT_NAME = 'instafun_api'
api_blueprint = Blueprint(API_BLUEPRINT_NAME, __name__)
#endregion create blueprint


#region the controller(s)
from auth import *
from post import *
from sse import *
#endregion the controller(s)
