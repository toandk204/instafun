from common import *
from app import requireValueFromRequest
from app import markAsLoggedIn
from app.model.user import User


#region web sign in
@api_blueprint.route('/auth/web/sign-in', methods=['POST'])
def auth_web_sign_in():
  email     = requireValueFromRequest(request, 'email')
  password  = requireValueFromRequest(request, 'password')

  #TODO we will put in more complicated authentication later
  ok=False
  if False \
    or (email=='user@instafun.com' and password=='user@instafun.com') \
    or (email=='andy@instafun.com' and password=='andy@instafun.com') \
    or (email=='bob@instafun.com' and password=='bob@instafun.com') \
  :
    ok=True

  user = User.query.filter_by(email=email).first()
  markAsLoggedIn(user)

  #output
  r = ApiResult()
  r.status = 1 if ok else 0

  okText = 'succeeded' if ok else 'failed'
  r.message = 'Authentication for web %s' % okText

  r.data = None

  return r.toJSON()

pass
#endregion web sign in
