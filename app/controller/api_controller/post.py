from common import *

from app import \
  requireValueFromRequest, \
  friendlyTimestamp, \
  s3, \
  currentUser, hasLoggedIn

from flask import render_template

from app.model.post import Post
from app.model.user import User
from app.model.like import Like
from app.model.comment import Comment


#region post's like
pass


@api_blueprint.route('/post/update-like', methods=['POST'])
def post_update_like():
  post_id = int(requireValueFromRequest(request, 'post_id'))
  user_id = int(requireValueFromRequest(request, 'user_id'))
  isLike  = int(requireValueFromRequest(request, 'isLike'))

  post = Post.query.get(post_id)
  user = User.query.get(user_id)

  if not post: raise Exception('Invalid post id=%s' % post_id)
  if not user: raise Exception('Invalid user id=%s' % user_id)

  #do update like for the post
  if isLike:
    Like.addLike(post_id, user_id)
  else:
    Like.removeLike(post_id, user_id)

  #output
  r = ApiResult()
  r.status = 1
  r.message = '%s like for user id=%s for post id=%s' % (
    'Add' if isLike==1 else 'Remove',
    user_id, post_id
  )
  r.data = None

  return r.toJSON()


@api_blueprint.route('/post/get-like', methods=['POST'])
def post_get_like():
  #parse resultAsHTML param
  resultAsHTML = int(requireValueFromRequest(request, 'resultAsHTML'))
  if not resultAsHTML: resultAsHTML = 0

  #parse post id param
  post_id = requireValueFromRequest(request, 'post_id', allowEmpty=True)
  post_id = int(post_id) if post_id else None

  if not post_id:
    likes=[]
  else:
    post = Post.query.get(post_id)
    if not post: raise Exception('Invalid post id=%s' % post_id)

    #load comment
    likes = post.getLike(mostRecentLimit=None)

  #output
  r = ApiResult()
  r.status = 1
  if resultAsHTML:
    r.data = render_template('feed/_like.html', **locals())

  else:
    #output JSON result
    r.data = [like.toDict() for like in likes]

  return r.toJSON()


pass
#endregion post's like


#region post's comment
pass


@api_blueprint.route('/post/get-comment', methods=['POST'])
def post_get_comment():
  # TODO only load the first 5, other need to click on 'View more' to see#}
  pass

  #parse post id param
  post_id = requireValueFromRequest(request, 'post_id', allowEmpty=True)
  post_id = int(post_id) if post_id else None

  if not post_id:
    #empty id, do nothing
    r = ApiResult()
    r.status = 1
    r.message = 'Nothing to do'
    return r.toJSON()

  post = Post.query.get(post_id)
  if not post: raise Exception('Invalid post id=%s' % post_id)

  resultAsHTML = int(requireValueFromRequest(request, 'resultAsHTML'))
  if not resultAsHTML: resultAsHTML=0

  #load comment
  comments = post.getComment(mostRecentLimit=None)

  #output
  r = ApiResult()
  r.status = 1
  if resultAsHTML:
    r.data = render_template('feed/_comment-list.html', **locals())

  else:
    #output JSON result
    r.data = [comment.toDict() for comment in comments]

  return r.toJSON()


@api_blueprint.route('/post/add-comment', methods=['POST'])
def post_add_comment():
  # TODO only load the first 5, other need to click on 'View more' to see#}
  pass

  #parse params
  post_id = int(requireValueFromRequest(request, 'post_id'))
  user_id = int(requireValueFromRequest(request, 'user_id'))
  commentText = requireValueFromRequest(request, 'commentText')
  resultAsHTML = requireValueFromRequest(request, 'resultAsHTML')

  if not resultAsHTML: resultAsHTML=0
  if not commentText: raise Exception('Empty comment not allowed')

  post = Post.query.get(post_id)
  if not post: raise Exception('Invalid post id=%s' % post_id)

  user = User.query.get(user_id)
  if not user: raise Exception('Invalid user id=%s' % user_id)

  #do add comment
  comment = Comment()
  comment.post_id = post_id
  comment.user_id = user_id
  comment.content = commentText
  db.session.add(comment)
  db.session.commit()

  #output
  r = ApiResult()
  r.status = 1
  if not resultAsHTML:
    r.data = comment.toDict()
  else:
    commentText = commentText
    userAvatar = user.avatar
    commentTime = friendlyTimestamp(comment.created_at)
    r.data = render_template('feed/_comment-solo.html', **locals())

  return r.toJSON()


pass
#endregion post's comment


def allowedImageType(filename):
  ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif', 'svg'])
  return True \
         and '.' in filename \
         and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@api_blueprint.route('/post/add', methods=['POST'])
def post_add():
  #parse params
  user_id = int(requireValueFromRequest(request, 'user_id'))
  description = requireValueFromRequest(request, 'description', allowEmpty=True)
  if not description: description='' #set description to empty to dodge 'None' value


  ##region handle uploaded image ref. http://flask.pocoo.org/docs/0.12/patterns/fileuploads/
  pass

  #region validation image
  errorMsg = "Upload image as filed 'file' is required"

  #check if no file uploaded
  if 'file' not in request.files: raise Exception(errorMsg)

  uploadedImage = request.files['file']
  if not uploadedImage: raise Exception(errorMsg)

  #check if user does not select file, browser also submit a empty part without filename
  if uploadedImage.filename == '': raise Exception("%s - no file selected" % errorMsg)

  #check if file type is allowed one
  if not allowedImageType(uploadedImage.filename): raise Exception(errorMsg)

  #endregion validation

  #save to temp file ref. https://stackoverflow.com/a/8577225/248616
  ext = uploadedImage.filename.split('.')[-1]
  import tempfile
  f, localPhotoFile = tempfile.mkstemp()
  localPhotoFile += '.' + ext
  uploadedImage.save(localPhotoFile)

  #upload 'filename' to S3 bucket
  imageOnS3 = s3.uploadPostPhoto(localPhotoFile)

  ##endregion


  user = User.query.get(user_id)
  if not user: raise Exception('Invalid user id=%s' % user_id)


  #do add new post
  post = Post()
  post.user_id = user_id
  post.description = description
  post.image_url = imageOnS3
  db.session.add(post)
  db.session.commit()
  #TODO how to refresh object 'post' so that we have the post.id value

  #output
  r = ApiResult()
  r.status = 1
  r.data = post.toDict()

  return r.toJSON()


@api_blueprint.route('/post/update-description', methods=['POST'])
def post_update_description():
  #parse params
  post_id = int(requireValueFromRequest(request, 'post_id'))
  user_id = int(requireValueFromRequest(request, 'user_id'))
  description = requireValueFromRequest(request, 'description', allowEmpty=True)
  if not description: description='' #set description to empty to dodge 'None' value

  user = User.query.get(user_id)
  if not user: raise Exception('Invalid user id=%s' % user_id)

  post = Post.query.get(post_id)
  if not post: raise Exception('Invalid post id=%s' % post_id)

  #ensure user is owner
  if user.id != post.user.id: raise Exception('Invalid owner user_id=%s for post id=%s' % (user_id, post.id) )


  #do update desc
  post.description = description
  db.session.add(post)
  db.session.commit()

  #output
  r = ApiResult()
  r.status = 1
  r.data = post.toDict()

  return r.toJSON()


@api_blueprint.route('/post/list', methods=['POST'])
def post_list():
  #require log in
  if not hasLoggedIn(): raise Exception('Please log in first')

  #parse params
  mostRecentLimit = requireValueFromRequest(request, 'mostRecentLimit', allowEmpty=True) ;
  mostRecentLimit = int(mostRecentLimit) if mostRecentLimit else None

  resultAsHTML = requireValueFromRequest(request, 'resultAsHTML', allowEmpty=True) ;
  resultAsHTML = int(resultAsHTML) if resultAsHTML else None

  #do listing
  posts = Post.list(mostRecentLimit)

  #output
  r = ApiResult()
  r.status = 1

  if resultAsHTML==1:
    r.data = render_template('feed/_post.html', **locals())
  else:
    r.data = [post.toDict() for post in posts]

  return r.toJSON()


@api_blueprint.route('/post/delete', methods=['POST'])
def post_delete():
  #parse params
  post_id = requireValueFromRequest(request, 'post_id', allowEmpty=True)
  post_id = int(post_id) if post_id else None

  if not post_id:
    #empty id, do nothing
    r = ApiResult()
    r.status = 1
    r.message = 'Nothing to do'
    return r.toJSON()

  #do add new post
  post = Post.query.get(post_id)
  if not post: raise Exception('Invalid post id=%s' % post_id)

  db.session.delete(post)
  db.session.commit()

  #output
  r = ApiResult()
  r.status = 1
  r.data = post.toDict()
  r.message = 'Post deleted id=%s' % post_id

  return r.toJSON()


pass
