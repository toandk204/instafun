def callAPI(endpoint, data={}, method='GET'):
  """
  - Make request to API from within Flask web app
  """

  ##region make request to api
  from app import API_HOST, API_BLUEPRINT_PREFIX
  url = 'http://{API_HOST}/{API_BLUEPRINT_PREFIX}/{endpoint}'.format(
    API_HOST=API_HOST,
    API_BLUEPRINT_PREFIX=API_BLUEPRINT_PREFIX,
    endpoint=endpoint
  )

  #send request
  import requests
  if method=='GET':
    r = requests.get(url=url, params=data)
  elif method=='POST':
    r = requests.post(url=url, params=data)

  #check for error
  r.raise_for_status()

  #output
  from api_result import ApiResult
  r = ApiResult.load(r)
  return r.data

  ##endregion make request to api
