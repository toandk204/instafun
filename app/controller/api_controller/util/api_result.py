class ApiResult:
  """
  Returned JSON object format
  ---------------------------
  ```
  {
    "status"  : "0", # 0/1 i.e. fail/succeed
    "message" : "",  # message when fail/succeed
    "errors"  : []   #error array to debug when status=0
    #other endpoint-specific fields
  }
  ```
  """

  status = 0
  data = ''
  message = None


  def __init__(self, status=0, message='', data=''):
    self.status = status
    self.message = message
    self.data = data


  def toJSON(self):
    """convert to json string from dict"""

    d = self.__dict__

    #remove `message` if it is empty
    if not d['message']:
      d.pop('message')

    from flask import jsonify
    return jsonify(d) # convert to json string from dict i.e. return JSON in Flask ref. http://stackoverflow.com/a/13089975/248616
