#the app instance
from flask import Flask

app = Flask(__name__)

#init session
app.config['SESSION_TYPE'] = 'filesystem'
app.secret_key = 'QS1pMSm2G4Mx3fH4uXk'


##region app config
pass

#base config
app.config.from_object('config.BaseConfig')

#region load local config to overwrite db entry, api host entry
try:
  from config_local import config_local
  app.config.update(config_local)  #append app config with the local one
except:
  pass #just keep on as no local config
#endregion


#region define page name table
@app.before_first_request
def app_before_first_request():
  from flask import url_for
  app.config['page'] = dict(
    USER_HOME=url_for('app_index'),
  )
#endregion define page name table

pass
##endregion app config


##region create the sqlalchemy object `db`

#build connection url
app.config['SQLALCHEMY_DATABASE_URI'] = \
  'mysql+pymysql://{DB_USER}:{DB_PASS}@localhost/{DB_NAME}?charset=utf8'\
  .format(DB_USER=app.config['DB_USER'], DB_PASS=app.config['DB_PASS'], DB_NAME=app.config['DB_NAME'])

#create db instance
from flask_sqlalchemy import SQLAlchemy
db = SQLAlchemy()
db.init_app(app)

##endregion create the sqlalchemy object `db`


##region utility
pass

from flask import \
  session, \
  render_template, redirect, url_for, \
  g, json, \
  request

import requests
from sqlalchemy import desc, asc

from datetime import datetime


from util import *

#register python function with jinja
app.jinja_env.globals.update(hasLoggedIn=hasLoggedIn)
app.jinja_env.globals.update(friendlyTimestamp=friendlyTimestamp)
app.jinja_env.globals.update(currentUser=currentUser)


#region S3 util
pass

#create S3 instance
from util.aws_s3 import AWS_S3
s3 = AWS_S3()


#open S3 connection
@app.before_first_request
def app_open_S3_connection():
  s3.connect()

#endregion S3 util


#region SSE util

#ref. http://flask-sse.readthedocs.io/en/latest/quickstart.html
pass

from flask_sse import sse as SSE_INSTANCE
app.config["REDIS_URL"] = "redis://localhost"
app.register_blueprint(SSE_INSTANCE, url_prefix='/stream')

from util.sse import *


@app.after_request
def app_after_request(response):
  """
  We try to fire SSE event post-update whenever a post info is updated e.g. new comment, new like, newly created
  Only need to guard post's changes via API endpoints
  """

  #ensure we handle API request only
  try:
    blueprint = request.endpoint.split('.')[0]
  except:
    blueprint = None  # no blueprint in endpoint, set it none

  from app.controller.api_controller import API_BLUEPRINT_NAME
  isAPIReuqest = blueprint == API_BLUEPRINT_NAME
  if not isAPIReuqest: return response

  #check endpoint in make-update-to-post list
  UPDATE_POST_ENDPOINT_LIST = [
    'post_add',
    'post_update_like',
    'post_add_comment',
    'post_delete',
    'post_update_description',
  ]
  endpoint = request.endpoint.split('.')[1]
  if endpoint in UPDATE_POST_ENDPOINT_LIST:
    #publish sse post-update event
    sse_publishPostUpdate(SSE_INSTANCE)

  return response #handling after-request in Flask ref. https://stackoverflow.com/a/31226309/248616

#endregion SSE util


pass
##endregion utility


###region load controller(s)

#web controller
from app.controller.web_controller import * #this import command will auto-import all modules under web_controller package

##region api controller as blueprint
pass

#api config
API_HOST = app.config['API_HOST']
API_BLUEPRINT_PREFIX = app.config['API_BLUEPRINT_PREFIX']

#api controller
from controller.api_controller.util.api_result import ApiResult
from controller.api_controller.util import *
from app.controller.api_controller import api_blueprint
app.register_blueprint(api_blueprint, url_prefix='/'+API_BLUEPRINT_PREFIX)

##endregion api controller as blueprint

pass
###endregion load controller(s)


#region error handler
@app.errorhandler(Exception)
def api_blueprint_error_handler(e):
  """format error as JSON if the request is from api module/blueprint"""

  #region check if current endpoint came from api blueprint
  try:
    blueprint = request.endpoint.split('.')[0]
  except:
    blueprint = None  # no blueprint in endpoint, set it none

  from app.controller.api_controller import API_BLUEPRINT_NAME
  isAPI = blueprint == API_BLUEPRINT_NAME

  #the request is NOT from API => skip it
  if not isAPI: return

  #endregion check if current endpoint came from api blueprint

  #print error track log to console ref. https://stackoverflow.com/a/14993650/248616
  from werkzeug.debug import get_current_traceback
  track = get_current_traceback(skip=1, show_hidden_frames=True, ignore_system_exceptions=False)
  track.log()

  from app.controller.api_controller.util.api_result import ApiResult
  r = ApiResult()
  r.status = 0
  r.message = "ERROR:\n%s\nTRACE:%s" % (e, track.plaintext)
  del r.data
  return r.toJSON()

pass
#endregion error handler
