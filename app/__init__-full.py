#region logging util
"""ref. https://gist.github.com/ibeex/3257877"""
import os
SCRIPT_HOME = os.path.abspath(os.path.dirname(__file__))
APP_LOG = '%s/static/app.log' % SCRIPT_HOME #note that log file place here can be accessed publicly

from logging.handlers import RotatingFileHandler
handler = RotatingFileHandler(APP_LOG, maxBytes=10000, backupCount=1)

import logging
handler.setLevel(logging.DEBUG)

formatter = logging.Formatter( ""
                               + "| %(levelname)s | %(message)s "
                               + "| %(funcName)s | %(pathname)s:%(lineno)d | %(asctime)s") #ref. https://gist.github.com/ibeex/3257877#gistcomment-1433750
handler.setFormatter(formatter)

app.logger.addHandler(handler)
#endregion logging util
