/*string.format() util ref. http://stackoverflow.com/a/20729945/248616*/
String.prototype.format = function() {
  var str = this;
  for (var i = 0; i < arguments.length; i++) {
    var reg = new RegExp("\\{" + i + "\\}", "gm");
    str = str.replace(reg, arguments[i]);
  }
  return str;
};


/*ajax error handler ref. http://stackoverflow.com/a/14563181/248616*/
function ajaxErrorHandler(jqXHR, exception) {
  var msg = '';
  if (jqXHR.status === 0) {
      msg = 'Not connect.\n Verify Network.';
  } else if (jqXHR.status == 404) {
      msg = 'Requested page not found. [404]';
  } else if (jqXHR.status == 500) {
      msg = 'Internal Server Error [500].';
  } else if (exception === 'parsererror') {
      msg = 'Requested JSON parse failed.';
  } else if (exception === 'timeout') {
      msg = 'Time out error.';
  } else if (exception === 'abort') {
      msg = 'Ajax request aborted.';
  } else {
      msg = 'Uncaught Error.\n' + jqXHR.responseText;
  }

  msg = 'ERROR\n'+msg;
  console.log(msg);
}


/*reload page*/
function reloadThisPage(response, params){
  ok = response.status===1;
  if (ok) {
    reloadPage(params);
  } else {
    console.error(response.message);
  }
}


function reloadPage(params){
  if (params) {
    /*reload same page with param ref. http://stackoverflow.com/a/12118862/248616*/
    window.location.href = window.location.href.replace(/[\?#].*|$/, '?'+params );
  } else {
    /*just reload page*/
    location.reload();
  }
}
