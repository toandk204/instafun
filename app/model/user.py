# -*- coding: utf-8 -*-
from base_model import BaseModel
from app import db


class User(BaseModel):

  __tablename__ = "users"

  #region mapped columns
  id = db.Column(db.INT, primary_key=True)
  email = db.Column(db.TEXT)
  password = db.Column(db.TEXT)
  avatar = db.Column(db.TEXT)
  #endregion mapped columns


  #region util
  pass

  def toDict(self):
    d = super(User, self).toDict()
    d.pop('password')
    return d


  def usernameFromEmail(self):
    if '@' in self.email:
      username = self.email.split('@')[0]
    else:
      username = self.email
    return username

  pass
  #endregion util
