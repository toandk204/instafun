# -*- coding: utf-8 -*-
from base_model import BaseModel
from app import db
from app.model.user import User


class Comment(BaseModel):

  __tablename__ = "comments"

  #region mapped columns
  id = db.Column(db.INT, primary_key=True)
  content = db.Column(db.TEXT)
  created_at = db.Column(db.TIMESTAMP, default=db.func.current_timestamp())

  user_id = db.Column(db.INT, db.ForeignKey(User.__table__.c.id) )
  user = db.relationship('User', primaryjoin='User.id==Comment.user_id')

  post_id = db.Column(db.INT, db.ForeignKey('posts.id') )
  post = db.relationship('Post', primaryjoin='Post.id==Comment.post_id')

  #endregion mapped columns
