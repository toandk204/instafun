# -*- coding: utf-8 -*-
from app import db
Base = db.Model


class BaseModel(Base):
  __abstract__ = True

  '''
  Common method for all model classes
  '''

  def __init__(self): pass


  def __repr__(self):
    columns = self._sa_class_manager.mapper.mapped_table.columns
    repr = [self.__tablename__]
    for col in columns:
      val = getattr(self, col.name)
      repr.append('{col}={val}'.format(col=col.name, val=val) )
    return ' '.join(repr)


  def toDict(self):
    d = {}
    columns = self._sa_class_manager.mapper.mapped_table.columns
    for col in columns:
      d[col.name] = getattr(self, col.name)
    return d
