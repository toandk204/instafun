# -*- coding: utf-8 -*-
from base_model import BaseModel
from app import db, desc
from app.model.user import User
from app.model.like import Like
from app.model.comment import Comment

from app import makeThumbnail, s3, getThumbnailOnS3


class Post(BaseModel):

  __tablename__ = "posts"

  #region mapped columns
  id = db.Column(db.INT, primary_key=True)
  description = db.Column(db.TEXT)
  image_url = db.Column(db.TEXT)
  created_at = db.Column(db.TIMESTAMP, default=db.func.current_timestamp())

  user_id = db.Column(db.INT, db.ForeignKey(User.__table__.c.id) )
  user = db.relationship('User', primaryjoin='User.id==Post.user_id')
  #endregion mapped columns

  #region util

  def getLikeCount(self):
    likes = Like.query.filter_by(post_id=self.id).all()
    return len(likes)


  def getCommentCount(self):
    return len(self.getComment())


  def getComment(self, mostRecentLimit=None):
    comments = Comment.query.filter_by(post_id=self.id)

    if mostRecentLimit is None:
      comments = comments.all()
    else:
      comments = comments.limit(mostRecentLimit)

    return comments


  def getLike(self, mostRecentLimit=None):
    likes = Like.query.filter_by(post_id=self.id)

    if mostRecentLimit is None:
      likes = likes.all()
    else:
      likes = likes.limit(mostRecentLimit)

    return likes


  @staticmethod
  def listAsJSON(mostRecentLimit=None):
    posts = Post.list(mostRecentLimit)
    posts = [post.toDict() for post in posts]
    return posts


  @staticmethod
  def list(mostRecentLimit=None):
    #sort post by created_at desc ref. https://stackoverflow.com/a/4186078/248616
    q = Post.query.order_by(desc(Post.created_at))

    #limit # of returned rows
    if mostRecentLimit is None:
      posts = q.all()
    else:
      posts = q.limit(mostRecentLimit).all()

    return posts


  def isLikedBy(self, userId):
    likes = Like.query.filter_by(post_id=self.id).all()
    for like in likes:
      if like.user_id == userId:
        return True
    return False

  #endregion util


  def isOwner(self, userId):
    return userId == self.user.id


  def thumbnail_url(self):
    #if it is .svg, just return itself
    if self.image_url.split('.')[-1] == 'svg':
      return self.image_url

    thumbUrl = getThumbnailOnS3(self.image_url, s3)
    if not thumbUrl:
      thumbUrl = makeThumbnail(self.image_url, s3)

    return thumbUrl
