# -*- coding: utf-8 -*-
from base_model import BaseModel
from app import db
from app.model.user import User


class Like(BaseModel):

  __tablename__ = "likes"

  #region mapped columns
  id = db.Column(db.INT, primary_key=True)
  created_at = db.Column(db.TIMESTAMP, default=db.func.current_timestamp())

  user_id = db.Column(db.INT, db.ForeignKey(User.__table__.c.id) )
  user = db.relationship('User', primaryjoin='User.id==Like.user_id')

  post_id = db.Column(db.INT, db.ForeignKey('posts.id') )
  post = db.relationship('Post', primaryjoin='Post.id==Like.post_id')

  #endregion mapped columns

  #region util
  @staticmethod
  def addLike(post_id, user_id):
    #check if already exists
    like = Like.query.filter_by(post_id=post_id, user_id=user_id).first()
    if like: return #don't need to add

    #not exists, do add
    like = Like()
    like.user_id = user_id
    like.post_id = post_id
    db.session.add(like)
    db.session.commit()


  @staticmethod
  def removeLike(post_id, user_id):
    #check if exists
    like = Like.query.filter_by(post_id=post_id, user_id=user_id).first()
    if not like: return #don't need to remove

    #do remove
    db.session.delete(like)
    db.session.commit()

  #endregion util
