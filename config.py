class BaseConfig(object):
  APP_NAME = 'instafun'

  DB_USER = 'root'
  DB_PASS = 'root'
  DB_NAME = 'instafun_LOCAL'

  API_HOST = 'localhost:5000'
  API_BLUEPRINT_PREFIX = 'api'

  SQLALCHEMY_TRACK_MODIFICATIONS = False #FSADeprecationWarning: SQLALCHEMY_TRACK_MODIFICATIONS adds significant overhead and will be disabled by default in the future.  Set it to True or False to suppress this warning.
