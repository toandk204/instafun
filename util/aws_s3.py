import boto
import boto.s3.connection


class AWS_S3:
  #TODO put below two keys into config_local
  #get key ref. https://console.aws.amazon.com/iam/home?region=ap-southeast-1#/security_credential
  AWS_ACCESS_KEY_ID     = 'AKIAJJ7F7BQGHXOJPYBA'
  AWS_SECRET_ACCESS_KEY = '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'
  AWS_REGION = 'ap-southeast-1' #region code ref. http://docs.aws.amazon.com/general/latest/gr/rande.html

  '''
  setup guide
  - install awscli
    $ pip install awscli
  
  - config after installed
    $ aws configure
    
  - aftermath test
    $ aws s3 ls
  '''

  BUCKET_NAME = 'nn-instafun'

  POST_IMAGE_FOLDER = 'image/post/'


  def __init__(self): pass

  conn = None
  bucket = None

  def connect(self):
    print 'Initial AWS S3 connection...'
    import inspect ; print inspect.stack()[1] #print caller info

    #open connection to non-us bucket ref. https://github.com/boto/boto/issues/443#issuecomment-227931193
    self.conn = boto.s3.connect_to_region(
      self.AWS_REGION,
      aws_access_key_id     = self.AWS_ACCESS_KEY_ID,
      aws_secret_access_key = self.AWS_SECRET_ACCESS_KEY,
      calling_format        = boto.s3.connection.OrdinaryCallingFormat()
    )
    self.bucket = self.conn.get_bucket(self.BUCKET_NAME)

    print 'Initial AWS S3 connection... DONE'


  def uploadPostPhoto(self, local_path, renamedFilename=None):
    return self.uploadFile(self.POST_IMAGE_FOLDER, local_path, renamedFilename)


  def uploadFile(self, folderPath, localPath, renamedFilename=None):
    #determine filename
    if renamedFilename: #another filename is favored, use it
      filename = renamedFilename

    else: #no favored filename exists, get it from the photo file
      import ntpath
      filename = ntpath.basename(localPath) #get filename from path ref. http://stackoverflow.com/a/8384788/248616

    ##region do upload
    f = open(localPath, 'rb')

    #make s3 key
    keyName = folderPath + filename
    key = self.bucket.new_key(keyName)

    #fill content
    key.set_contents_from_file(f)

    ##endregion do upload

    #do publish
    key.set_acl('public-read') #set published permission
    url = key.generate_url(expires_in=0, query_auth=False)

    return url

  #endregion get published url
