def sse_publishPostUpdate(SSE_INSTANCE):
  eventType = 'post-update'
  eventMsg = dict(message='')
  SSE_INSTANCE.publish(eventMsg, type=eventType)
  return eventType,eventMsg
