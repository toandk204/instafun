def getAuthInfo():
  from app import session
  return session.get('authInfo') #get session key ignoring if not exists ref. http://stackoverflow.com/a/28926347/248616


def hasLoggedIn():
  return getAuthInfo() is not None


def currentUser():
  from app import session
  try:
    return session['authInfo']['user']
  except:
    return None


def markAsLoggedIn(user):
  from app import session
  session['authInfo'] = dict(
    user = user.toDict()
  )


def requireValueFromRequest(request, field, allowEmpty=False):
  value = request.values.get(field)
  if value: return value

  if not value: #value not found
    #region raise error if required i.e. allowEmpty=False
    if not allowEmpty:
      raise Exception('Param `%s` is required' % field)

    else: #return None when empty is a need, do it
      return None

    pass
    #endregion raise error if required i.e. allowEmpty=False


def friendlyTimestamp(time=False):
  """
  Get a datetime object or a int() Epoch timestamp and return a
  pretty string like 'an hour ago', 'Yesterday', '3 months ago',
  'just now', etc
  ref. https://stackoverflow.com/a/1551394/248616
  """
  from datetime import datetime
  now = datetime.now()
  if type(time) is int:
    diff = now - datetime.fromtimestamp(time)
  elif isinstance(time, datetime):
    diff = now - time
  elif not time:
    diff = now - now
  second_diff = diff.seconds
  day_diff = diff.days

  if day_diff < 0:
    return ''

  if day_diff == 0:
    if second_diff < 10:
      return "just now"
    if second_diff < 60:
      return str(second_diff) + " seconds ago"
    if second_diff < 120:
      return "a minute ago"
    if second_diff < 3600:
      return str(second_diff / 60) + " minutes ago"
    if second_diff < 7200:
      return "an hour ago"
    if second_diff < 86400:
      return str(second_diff / 3600) + " hours ago"
  if day_diff == 1:
    return "Yesterday"
  if day_diff < 7:
    return str(day_diff) + " days ago"
  if day_diff < 31:
    return str(day_diff / 7) + " weeks ago"
  if day_diff < 365:
    return str(day_diff / 30) + " months ago"
  return str(day_diff / 365) + " years ago"


#region image thumbnail
pass


def makeThumbnail(originImageUrl, s3):
  #get image file name & extension ref. https://stackoverflow.com/a/2795396/248616
  import urlparse
  split = urlparse.urlsplit(originImageUrl)
  filename = split.path.split('/')[-1]
  [name,extension] = filename.split('.')

  #prepare thumb filename
  from PIL import Image
  thumbImageFile = '/tmp/%s-thumb.%s' % (name,extension)

  #download origin image
  originImageFile = '/tmp/%s' % filename
  import urllib
  urllib.urlretrieve(originImageUrl, originImageFile) #download image ref. https://stackoverflow.com/a/3042786/248616

  #create thumbnail
  image = Image.open(originImageFile)
  size = (440,440)
  image.thumbnail(size, Image.ANTIALIAS) #helper to ease the ratio issue ref. https://stackoverflow.com/a/940368/248616
  image.save(thumbImageFile)

  #upload to s3
  thumbImageUrl = s3.uploadPostPhoto(thumbImageFile)
  return thumbImageUrl


def getThumbnailOnS3(image_url, s3):
  #ref. https://stackoverflow.com/a/7933570/248616
  #format https://<bucket-name>.s3.amazonaws.com/<key>

  keyName = image_url.split('/')[-1]
  key = s3.bucket.get_key(keyName)
  if not key: return None

  url = key.generate_url(expires_in=0, query_auth=False) #get url from key ref. https://stackoverflow.com/a/16158333/248616
  return url


pass
#endregion image thumbnail
