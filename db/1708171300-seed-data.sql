SET NAMES utf8;

/*
NOTE:
Our seeding records require resources on S3 bucket to be well-prepared at
https://s3-ap-southeast-1.amazonaws.com/nn-instafun/
*/


DELETE FROM users;
INSERT INTO users(email, password, avatar) VALUES
  ('user@instafun.com', 'user@instafun.com', 'https://s3-ap-southeast-1.amazonaws.com/nn-instafun/image/user/user-gray.png')
, ('andy@instafun.com', 'andy@instafun.com', 'https://s3-ap-southeast-1.amazonaws.com/nn-instafun/image/user/andy.png')
, ('bob@instafun.com', 'bob@instafun.com', 'https://s3-ap-southeast-1.amazonaws.com/nn-instafun/image/user/bob.png')
;

set @userId = (select id from users where email like 'user%');
set @andyId = (select id from users where email like 'andy%');
set @bobId  = (select id from users where email like 'bob%');


DELETE FROM posts;
INSERT INTO posts(user_id, description, image_url) VALUES
  (@andyId,
   'Duis postAndy01 rhoncus rutrum convallis. Aenean vitae orci nec diam porttitor pharetra. Nunc commodo tellus ut justo ultrices, quis egestas nibh venenatis.',
   'https://s3-ap-southeast-1.amazonaws.com/nn-instafun/image/post/post-andy01.png')
, (@bobId,
   'Etiam postBob01 quis elit molestie, ullamcorper justo ut, fermentum sapien. Sed porttitor ut ex eu fringilla.',
   'https://s3-ap-southeast-1.amazonaws.com/nn-instafun/image/post/post-bob01.png')
, (@userId,
   'Mauris egestas odio ac felis porta rutrum.',
   'https://s3-ap-southeast-1.amazonaws.com/nn-instafun/image/post/post-user01.svg')
;

set @postAndy01 = (select id from posts where description like '%postAndy01%' limit 1);
set @postBob01  = (select id from posts where description like '%postBob01%' limit 1);


DELETE FROM comments;
INSERT INTO comments(post_id, user_id, content) VALUES
  (@postAndy01, @userId,  'Donec vitae ipsum a nunc hendrerit volutpat.')
, (@postAndy01, @andyId,  'Curabitur pellentesque iaculis nulla.')
, (@postAndy01, @bobId,   'Etiam turpis nisi, vulputate vel condimentum in, dapibus ut metus.')

, (@postBob01, @andyId,  'Curabitur ut ante in nisl laoreet placerat tempus sit amet sapien.')
, (@postBob01, @bobId,   'Cras interdum ligula eu placerat convallis. In in mi fermentum, bibendum nulla in, euismod mauris.')
, (@postBob01, @userId,  'Curabitur leo massa, pulvinar eu accumsan tempus, sagittis a justo. Sed enim dui, gravida a varius sit amet, scelerisque eget nisl.')

;


DELETE FROM likes;
INSERT INTO likes(user_id, post_id) VALUES
  (@bobId,  @postAndy01)
, (@userId, @postAndy01)
;
