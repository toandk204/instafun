drop database if exists `instafun_LOCAL`;
create database `instafun_LOCAL` default character set utf8;

drop user if exists `instafun`;
create user `instafun`@'%' identified by 'instafun';
grant all on *.* to `instafun`@'%';
