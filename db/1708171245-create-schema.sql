DROP TABLE if exists users;

CREATE TABLE users (
  id int AUTO_INCREMENT,
  email text CHARACTER SET utf8,
  avatar text CHARACTER SET utf8,
  password text CHARACTER SET utf8,
  PRIMARY KEY (id)
);


DROP TABLE if exists posts;

CREATE TABLE posts (
  id int AUTO_INCREMENT,
  user_id int REFERENCES users(id),
  description text CHARACTER SET utf8,
  image_url text CHARACTER SET utf8,
  created_at TIMESTAMP default CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
);


DROP TABLE if exists comments;

CREATE TABLE comments (
  id int AUTO_INCREMENT,
  user_id int REFERENCES users(id),
  post_id int REFERENCES posts(id),
  content text CHARACTER SET utf8,
  created_at TIMESTAMP default CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
);


DROP TABLE if exists likes;

CREATE TABLE likes (
  id int AUTO_INCREMENT,
  user_id int REFERENCES users(id),
  post_id int REFERENCES posts(id),
  created_at TIMESTAMP default CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
);
