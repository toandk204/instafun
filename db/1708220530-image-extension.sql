update posts 
  set image_url=concat(image_url,".","png")
;

update posts
  set image_url=replace(image_url, ".png.png", ".png")
;

update posts
  set image_url=replace(image_url, ".svg.png", ".svg")
;

set @postId = (
  select id from posts where image_url = 'https://s3-ap-southeast-1.amazonaws.com/nn-instafun/image/post/tmpLQV7tF.png'
);
update posts
  set image_url = 'https://s3-ap-southeast-1.amazonaws.com/nn-instafun/image/post/tmpLQV7tF.jpeg'
  where id = @postId
;
