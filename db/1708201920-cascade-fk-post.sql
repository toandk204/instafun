ALTER TABLE likes
  ADD CONSTRAINT fk_likes_posts
  FOREIGN KEY (post_id) REFERENCES posts(id)
   ON DELETE CASCADE
;

ALTER TABLE comments
  ADD CONSTRAINT fk_comments_posts
  FOREIGN KEY (post_id) REFERENCES posts(id)
   ON DELETE CASCADE
;
